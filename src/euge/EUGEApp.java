package euge;

import euge.interfaces.*;

public class EUGEApp {

	EUGEContext context;
	EUGETimer timer;
	EUGEProc proc;
		
	boolean stop = false;

	public void setProc(EUGEProc p) {
		context.setProc(p);
		proc = p;
	}
	
	public void stop() {
		stop = true;
	}

	public EUGEApp(EUGEContext context) {
		this.context = context;
		timer = context.getTimer();
		context.setParent(this);
	}

	public void setup(int x, int y, int height, int width) {
		context.setSize(height, width);
		context.setPosition(x, y);
	}
	
	public EUGETimer getTimer() {
		return timer;
	}

	public void start() {
		context.display(true);
		while (!stop) {
			timer.sleep();
			proc.update();
			context.repaint();
		}
	}
}
