package euge.demo.sample;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import euge.*;
import euge.backends.j2d.*;
import euge.interfaces.EUGEGraphics;
import euge.interfaces.EUGEProc;

public class Main implements EUGEProc {
	EUGEApp app;
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		// 建立一個應用程式
		app = new EUGEApp(new J2DContext("Window"));
		
		// 設定視窗大小
		app.setup(300, 100, 800, 600);
		
		// 註冊 callback
		app.setProc(this);
		
		// 設定 FPS
		app.getTimer().setFPS(60);
		
		// 啓動應用程式，進入迴圈
		app.start();
	}
	
	@Override
	public void update() {

	}

	@Override
	public void render(Graphics2D g) {
		g.setColor(Color.blue);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawLine(0, 0, 800, 600);
	}
}