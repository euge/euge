package euge.interfaces;

import java.awt.Graphics2D;

public interface EUGEProc {
	public void update();
	public void render(Graphics2D g);
}
