package euge.interfaces;

import euge.EUGEApp;

public interface EUGEContext {
    public EUGETimer getTimer();
    public EUGESound getSound();
    public EUGEInput getInput();
    public void setProc(EUGEProc p);
    public void setSize(int height,int width);
    public void setPosition(int x, int y);
    public int[] getSize();
    public int[] getPosition();
    public void display(boolean b);
    public boolean getdisplay();
    public void repaint();
	public void setParent(EUGEApp p);
}
