package euge.interfaces;

import java.awt.Graphics2D;
import java.awt.Image;

public interface EUGEGraphics{
    public void drawImage(Image img, int x, int y);
    
    /**
     * 取得 Graphics2D ，如無 Graphics2D 實作，則以 BufferedImage 建立，並於繪圖後，再把 BufferedImage
     * 貼上螢幕。
     * @return
     */
    public Graphics2D getAWTGraphics2D();
}
