package euge.interfaces;

public interface EUGETimer {
    public void setFPS(int fps);
    public int getFPS();
    public int getRealFPS();
	public void sleep();
}
