package euge.backends.j2d;

import euge.EUGEApp;
import euge.interfaces.EUGEContext;
import java.awt.event.*;
import euge.interfaces.EUGEInput;
import euge.interfaces.EUGEProc;
import euge.interfaces.EUGESound;
import euge.interfaces.EUGETimer;
import java.awt.*;

/**
 * Java 2D
 * 
 * @author Jay
 * 
 */
public class J2DContext implements EUGEContext {
	Frame frame;
	DCanvas canvas;
	EUGEProc proc;
	EUGEApp parent;

	@Override
	public void setParent(EUGEApp p) {
		parent = p;
	}

	public J2DContext(String name) {
		frame = new Frame(name);
		canvas = new DCanvas();
		frame.add(canvas);
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (parent != null)
					parent.stop();
				frame.dispose();
			}
		});

	}

	class DCanvas extends Canvas {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Image buffer;
		Graphics2D bufferG;

		@Override
		public void update(Graphics g) {
			paint(g);
		}

		public void newBuffer() {
			if (bufferG != null) {
				bufferG.dispose();
				bufferG = null;
			}
			if (buffer != null) {
				buffer.flush();
				buffer = null;
			}
			buffer = createVolatileImage(getSize().width, getSize().height);
			bufferG = (Graphics2D) buffer.getGraphics();

		}

		@Override
		public void paint(Graphics g) {
			if (buffer == null || bufferG == null)
				newBuffer();

			if (bufferG != null) {
				bufferG.clearRect(0, 0, getSize().width, getSize().height);
				paintBuffer(bufferG);
				g.drawImage(buffer, 0, 0, null);
			}

		}

		public void paintBuffer(Graphics2D g) {
			if (proc != null)
				proc.render(g);
		}
	}

	@Override
	public void repaint() {
		canvas.repaint();
	}

	@Override
	public EUGETimer getTimer() {
		return new J2DTimer(this);
	}

	@Override
	public EUGESound getSound() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EUGEInput getInput() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSize(int height, int width) {
		canvas.setSize(height, width);
		frame.pack();
		canvas.newBuffer();
	}

	@Override
	public void setPosition(int x, int y) {
		frame.setLocation(x, y);

	}

	@Override
	public void display(boolean b) {
		frame.setVisible(true);
	}

	@Override
	public int[] getSize() {
		int[] i = { canvas.getSize().height, canvas.getSize().width };
		return i;
	}

	@Override
	public int[] getPosition() {
		int[] i = { frame.getLocation().x, frame.getLocation().y };
		return i;
	}

	@Override
	public boolean getdisplay() {
		return frame.isActive();
	}

	@Override
	public void setProc(EUGEProc p) {
		proc = p;
	}
}
