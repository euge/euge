package euge.backends.j2d;

import euge.interfaces.EUGETimer;

/**
 * J2DTimer 用於 Java 2D 實作的 Timer。
 * 
 * @author Jay
 * 
 */
class J2DTimer implements EUGETimer {

	int fps;
	int rfps;
	J2DContext context;

	// 開始時的時間
	long begin = System.nanoTime();

	// 下次要更新畫面的時間
	long next;

	J2DTimer(J2DContext context) {
		fps = 60;
		this.context = context;
	}

	@Override
	public void setFPS(int fps) {
		this.fps = fps;
	}

	@Override
	public int getFPS() {
		return fps;
	}

	@Override
	public int getRealFPS() {
		// 取得真際上的FPS
		return 0;
	}

	public void sleep() {
		long now = System.nanoTime();

		// 此情況幾乎不會發生，但如果時間太長還是有可能發生。
		if (now < begin)
			begin = now;

		// 此情況只有開始時會發生(next = o) (或過了很久?)
		if (next < begin)
			next = now + (1000000000 / fps);

		if ((now - next) > (1000000000 / fps)) {
			/*
			 * 由於電腦處理速過慢，因此已慢超過了等待時間，
			 * 因此現在就處理下一個任務。
			 */
			next = now;
			return;
		} else if ((next - now) > (1000000000 / fps)) {
			next = now + (1000000000 / fps);
		} else {
			// 下次要動作的時間。
			next = next + (1000000000 / fps);
		}
		try {
			
			//暫停
			Thread.sleep((next - now) / 1000000);

		} catch (InterruptedException e) {

		}
	}

}
