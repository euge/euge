package euge.backends.j2d;

import euge.interfaces.EUGEGraphics;
import java.awt.*;

class J2DGraphics implements EUGEGraphics {
	Graphics2D g;

	public J2DGraphics(Graphics2D g) {
		this.g = g;
	}

	@Override
	public Graphics2D getAWTGraphics2D() {
		return g;
	}

	@Override
	public void drawImage(Image img, int x, int y) {
		g.drawImage(img, x, y, null);
	}

}
